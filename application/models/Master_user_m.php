<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_user_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_user';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'username',  //samakan dengan atribute name pada tags input
                'label' => 'username',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'userpass',
                'label' => 'Password',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll()
    {
        $this->db->select('a.*');
        $this->db->from('master_user a');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function save()
    {
        $data = array(
            "username" => $this->input->post('username'),
            "userpass" => md5($this->input->post('userpass')),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        $this->db->insert($this->table, $data);
    }

    // 14-06-2022
    public function get_user_by_id($id)
    {
        $this->db->select('a.*');
        $this->db->from('master_user a');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    // function jumlah_data(){

    //     return $this->db->get($this->table)->num_rows();
    // }

    // function data($number,$offset){
    //     $this->db->select('a.*, b.nama_grup, c.nama as prodi');
    //     $this->db->from('master_user a');
    //     $this->db->join('master_grup_user b', 'b.id = a.id_grup_user', 'left');
    //     $this->db->join('master_program_studi c', 'c.id = a.id_program_studi', 'left');
    //     $this->db->order_by("id", "desc");
    //     $query = $this->db->get($this->table,$number,$offset);   
    //     return $query->result();    
    // }
}
