<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stok_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'stok';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_agen',  //samakan dengan atribute name pada tags input
                'label' => 'Agen',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'kd_barang',  //samakan dengan atribute name pada tags input
                'label' => 'Kode Barang',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nama',  //samakan dengan atribute name pada tags input
                'label' => 'Nama Barang',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'artikel',  //samakan dengan atribute name pada tags input
                'label' => 'Artikel',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'harga',  //samakan dengan atribute name pada tags input
                'label' => 'Harga',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'stok',  //samakan dengan atribute name pada tags input
                'label' => 'stok',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ]
        ];
    }

    public function getAll($agen)
    {
        $this->db->select('a.*, b.nama as agen');
        $this->db->from('stok a');
        $this->db->join('agen b','b.id = a.id_agen','left');
        if ($agen != NULL)
            $this->db->where("a.id_agen", $agen);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getAgen()
    {
        return $this->db->get('agen')->result_array();
    }

    public function save()
    {
        $data = array(
            "id_agen" => $this->input->post('id_agen'),
            "kd_barang" => $this->input->post('kd_barang'),
            "nama" => $this->input->post('nama'),
            "artikel" => $this->input->post('artikel'),
            "harga" => $this->input->post('harga'),
            "stok" => $this->input->post('stok'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

}