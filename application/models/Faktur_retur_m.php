<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Faktur_retur_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'faktur_retur';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_agen',  //samakan dengan atribute name pada tags input
                'label' => 'Agen',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'saldo_akhir',  //samakan dengan atribute name pada tags input
                'label' => 'Saldo Ahir',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'harga',  //samakan dengan atribute name pada tags input
                'label' => 'Harga',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'qty',  //samakan dengan atribute name pada tags input
                'label' => 'QTY',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'transaksi',
                'label' => 'Transaksi',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama_sales',
                'label' => 'Nama Sales',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'no_faktur',
                'label' => 'No. Faktur',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama_rute',
                'label' => 'Nama Rute',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required'
            ]
        ];
    }

    public function getAll($agen)
    {
        $this->db->select('a.*, b.nama as agen');
        $this->db->from('faktur_retur a');
        $this->db->join('agen b','b.id = a.id_agen','left');
        if ($agen != NULL)
            $this->db->where("a.id_agen", $agen);
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getAgen()
    {
        return $this->db->get('agen')->result_array();
    }

    public function save()
    {
        $data = array(
            "id_agen" => $this->input->post('id_agen'),
            "saldo_akhir" => $this->input->post('saldo_akhir'),
            "harga" => $this->input->post('harga'),
            "qty" => $this->input->post('qty'),
            "transaksi" => $this->input->post('transaksi'),
            "nama_sales" => $this->input->post('nama_sales'),
            "no_faktur" => $this->input->post('no_faktur'),
            "nama_rute" => $this->input->post('nama_rute'),
            "status" => $this->input->post('status'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

}