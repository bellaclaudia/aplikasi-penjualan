<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'penjualan';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'qty',  //samakan dengan atribute name pada tags input
                'label' => 'QTY',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'tgl_pembelian',
                'label' => 'Tanggal Pembelian',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_pembelian',
                'label' => 'Tanggal Pembelian',
                'rules' => 'trim|required'
            ]
        ];
    }

    public function getAll($tgl_awal, $tgl_akhir, $agen)
    {
        $this->db->select('a.*, b.nama as nama_barang, sum(a.qty) as jml_qty, sum(a.jumlah) as total, c.nama as agen, c.alamat as alamat_agen, c.bank, c.atas_nama, c.no_rek, b.artikel');
        $this->db->from('penjualan a');
        $this->db->join('stok b', 'b.id = a.id_barang', 'left');
        $this->db->join('agen c', 'c.id = b.id_agen', 'left');
        if ($tgl_awal != NULL)
            $this->db->where("a.tgl_pembelian >=", $tgl_awal);
        if ($tgl_akhir != NULL)
            $this->db->where("a.tgl_pembelian <=", $tgl_akhir);
        if ($agen != NULL)
            $this->db->where("b.id_agen <=", $agen);
        $this->db->group_by("a.no_kwitansi");
        $this->db->order_by("a.no_kwitansi", "desc");

        $query = $this->db->get();
        return $query->result();
    }

    public function getExport($tgl_awal, $tgl_akhir, $agen)
    {
        $this->db->select('a.*, b.nama as nama_barang, b.harga, c.nama as agen, c.alamat as alamat_agen, c.bank, c.atas_nama, c.no_rek, b.artikel');
        $this->db->from('penjualan a');
        $this->db->join('stok b', 'b.id = a.id_barang', 'left');
        $this->db->join('agen c', 'c.id = b.id_agen', 'left');
        if ($tgl_awal != NULL)
            $this->db->where("a.tgl_pembelian >=", $tgl_awal);
        if ($tgl_akhir != NULL)
            $this->db->where("a.tgl_pembelian <=", $tgl_akhir);
        if ($agen != NULL)
            $this->db->where("b.id_agen <=", $agen);
        $this->db->order_by("a.tgl_pembelian", "desc");

        $query = $this->db->get();
        return $query->result();
    }

    public function getOne($no_kwitansi)
    {
        $this->db->select('a.*, b.nama as nama_barang, b.harga, a.jumlah, c.nama as agen, c.alamat as alamat_agen, c.bank, c.atas_nama, c.no_rek, b.artikel');
        $this->db->from('penjualan a');
        $this->db->join('stok b', 'b.id = a.id_barang', 'left');
        $this->db->join('agen c', 'c.id = b.id_agen', 'left');
        $this->db->where("a.no_kwitansi", $no_kwitansi);
        $this->db->where("a.qty !=", NULL);
        $this->db->where("a.jumlah !=", 0);
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query;
    }

    public function getAgen()
    {
        return $this->db->get('agen')->result_array();
    }

    public function getArtikel()
    {
        return $this->db->get('artikel')->result_array();
    }

    public function getStokBarang($id)
    {
        $this->db->where("id_agen", $id);
        $this->db->where("stok >", 0);
        $this->db->order_by("nama", "asc");
        return $this->db->get('stok')->result_array();
    }

    public function insert($data){
        $this->db->insert($this->table, $data);
    }

    public function getById($no_kwitansi)
    {
        return $this->db->get_where($this->table, ["no_kwitansi" => $no_kwitansi])->row_array();
    }

    function pilihPegawai($key=''){
        $this->db->select('id, kd_barang, nama, harga, stok, artikel');
        //$this->db->like('nofile',$key);
        $this->db->like('nama',$key);
        $this->db->or_like('nama',strtoupper($key));
        return $this->db->get('stok');
    }
}