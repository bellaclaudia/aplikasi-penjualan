<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Artikel_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'artikel';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'artikel',  //samakan dengan atribute name pada tags input
                'label' => 'Artikel',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ]
        ];
    }

    public function getAll()
    {
        $this->db->select('a.*');
        $this->db->from('artikel a');
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function save()
    {
        $data = array(
            "artikel" => $this->input->post('artikel'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

}