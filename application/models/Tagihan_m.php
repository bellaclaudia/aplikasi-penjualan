<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tagihan_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'tagihan';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_agen',  //samakan dengan atribute name pada tags input
                'label' => 'Agen',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'no_faktur',  //samakan dengan atribute name pada tags input
                'label' => 'No. Faktur',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'nota',  //samakan dengan atribute name pada tags input
                'label' => 'Nota',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'customer',  //samakan dengan atribute name pada tags input
                'label' => 'Customer',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'pertama',  //samakan dengan atribute name pada tags input
                'label' => '0 < 30',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'kedua',  //samakan dengan atribute name pada tags input
                'label' => '31 > 60',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'ketiga',
                'label' => '61 > 90',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'keempat',
                'label' => '91 > 120',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'kelima',
                'label' => '121 > 150',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'keenam',
                'label' => '> 150',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'jumlah',
                'label' => 'jumlah',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'bg',
                'label' => 'bg',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'total',
                'label' => 'total',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'fak',
                'label' => 'fak',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'ret',
                'label' => 'ret',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'pay',
                'label' => 'pay',
                'rules' => 'trim|required'
            ]
        ];
    }

    public function getAll($agen,$rute)
    {
        $this->db->select('a.*, c.nama as agen, c.alamat, sum(a.jumlah) as total');
        $this->db->from('tagihan a');
        $this->db->join('agen c','c.id = a.id_agen','left');
        if ($agen != NULL)
            $this->db->where("a.id_agen", $agen);
        if ($rute != NULL)
            $this->db->where("c.alamat", $rute);
        $this->db->order_by("a.nota", "desc");
        $this->db->group_by("a.nota");
        $query = $this->db->get();
        return $query->result();
    }

    public function getAgen()
    {
        return $this->db->get('agen')->result_array();
    }

    public function getRute()
    {
        $this->db->group_by("alamat");
        return $this->db->get('agen')->result_array();
    }

    public function getFakturRetur()
    {
        return $this->db->get('faktur_retur')->result_array();
    }

    public function getPenjualan()
    {
        $this->db->group_by("no_kwitansi");
        return $this->db->get('penjualan')->result_array();
    }

    public function save()
    {
        $data = array(
            "id_agen" => $this->input->post('id_agen'),
            "no_faktur" => $this->input->post('no_faktur'),
            "nota" => $this->input->post('nota'),
            "customer" => $this->input->post('customer'),
            // "pertama" => $this->input->post('pertama'),
            // "kedua" => $this->input->post('kedua'),
            // "ketiga" => $this->input->post('ketiga'),
            // "keempat" => $this->input->post('keempat'),
            // "kelima" => $this->input->post('kelima'),
            // "keenam" => $this->input->post('keenam'),
            "jumlah" => $this->input->post('jumlah'),
            // "bg" => $this->input->post('bg'),
            // "total" => $this->input->post('total'),
            // "fak" => $this->input->post('fak'),
            // "ret" => $this->input->post('ret'),
            // "pay" => $this->input->post('pay'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

}