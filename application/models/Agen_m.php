<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agen_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'agen';
    
    public function rules()
    {
        return [
            [
                'field' => 'nama',
                'label' => 'Nama Barang',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'bank',
                'label' => 'Bank',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'atas_nama',
                'label' => 'Atas Nama',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'no_rek',
                'label' => 'No. Rekening',
                'rules' => 'trim|required'
            ]
        ];
    }

    public function getAll()
    {
        $this->db->select('a.*');
        $this->db->from('agen a');
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function save()
    {
        $data = array(
            "nama" => $this->input->post('nama'),
            "alamat" => $this->input->post('alamat'),
            "bank" => $this->input->post('bank'),
            "atas_nama" => $this->input->post('atas_nama'),
            "no_rek" => $this->input->post('no_rek'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

}