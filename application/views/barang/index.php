<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Barang</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Barang
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarian', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tanggal Awal</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_awal">
                <small class="text-danger">
                  <?php echo form_error('tgl_awal') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tanggal Akhir</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_akhir">
                <small class="text-danger">
                  <?php echo form_error('tgl_akhir') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Agen</label>
              <div class="col-md-4">
                  <select class="form-control" id="select_page" name="agen">
                    <option value="" selected disabled>Pilih Agen</option>
                    <?php foreach ($data_agen as $k) : ?>
                      <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                    <?php endforeach; ?>
                  </select>
                  <small class="text-danger">
                    <?php echo form_error('agen') ?>
                  </small>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>
            </form>
            <hr>
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>
          <div style="overflow-x:auto;">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th></th>
                  <th>Agen</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Artikel</th>
                  <th>Qty</th>
                  <th>Harga</th>
                  <th>Total</th>
                  <th>Tanggal Masuk</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_barang as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->agen ?></td>
                    <td><?= $row->kd_barang ?></td>
                    <td><?= $row->nama ?></td>
                    <td><?= $row->artikel ?></td>
                    <td><?= $row->qty ?></td>
                    <td>
                      <?php if ($row->harga != '') { ?>
                      <?= "Rp " . number_format($row->harga,2,',','.'); ?>
                      <?php } ?>
                    </td>
                    <td>
                      <?php if ($row->harga != '') { ?>
                      <?= "Rp " . number_format($row->harga*$row->qty,2,',','.'); ?>
                      <?php } ?>
                    </td>
                    <td><?= date('d-m-Y', strtotime($row->tgl_masuk))?></td>
                    <td>
                      <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top">Edit</a>
                      <a href="<?php echo site_url('barang/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Barang <?= $row->nama; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data">Hapus</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddBarang', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Agen</label>
                    <div class="col-md-9">
                      <select class="form-control" id="select_page" name="id_agen">
                        <option value="0" selected disabled>Pilih Agen</option>
                        <?php foreach ($data_agen as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger">
                        <?php echo form_error('id_agen') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Kode Barang</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="kd_barang" placeholder="Kode Barang" required>
                      <small class="text-danger">
                        <?php echo form_error('kd_barang') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Nama Barang</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="nama" placeholder="Nama Barang" required>
                      <small class="text-danger">
                        <?php echo form_error('nama') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Artikel</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="artikel" placeholder="Artikel" required>
                      <small class="text-danger">
                        <?php echo form_error('artikel') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">QTY</label>
                    <div class="col-md-9">
                      <input class="form-control" type="number" name="qty" placeholder="QTY" required>
                      <small class="text-danger">
                        <?php echo form_error('qty') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Harga</label>
                    <div class="col-md-9">
                      <input class="form-control" type="number" name="harga" placeholder="Isi Harga" required>
                      <small class="text-danger">
                        <?php echo form_error('harga') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Tanggal Masuk</label>
                    <div class="col-md-9">
                      <input class="form-control" type="date" name="tgl_masuk" required>
                      <small class="text-danger">
                        <?php echo form_error('tgl_masuk') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_barang as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="FrmAdd" class="form-horizontal" action="<?php echo site_url('barang/edit/'.$row->id); ?>" method="post" enctype="multipart/form-data" autocomplete="off">
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Agen</label>
                        <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <select class="form-control" id="select_page" name="id_agen">
                            <option value="" selected disabled>Pilih Agen</option>
                            <?php
                              $lv = '';
                              if (isset($row->id_agen)) {
                                $lv = $row->id_agen;
                              }
                              foreach ($data_agen as $r => $v) {
                              ?>
                                <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                              <?php
                              }
                              ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('id_agen') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Kode Barang</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="kd_barang" placeholder="Kode Barang" value="<?= $row->kd_barang; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('kd_barang') ?>
                          </small>
                        </div>
                      </div>
  	                  <div class="form-group row">
  	                    <label class="col-md-3 col-form-label">Nama Barang</label>
  	                    <div class="col-md-9">
  	                      <input class="form-control" type="text" name="nama" placeholder="Nama Barang" value="<?= $row->nama; ?>" required>
  	                      <small class="text-danger">
  	                        <?php echo form_error('nama') ?>
  	                      </small>
  	                    </div>
  	                  </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Artikel</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="artikel" placeholder="Artikel" value="<?= $row->artikel; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('artikel') ?>
                          </small>
                        </div>
                      </div>
  	                  <div class="form-group row">
  	                    <label class="col-md-3 col-form-label">QTY</label>
  	                    <div class="col-md-9">
  	                      <input class="form-control" type="number" name="qty" placeholder="QTY" value="<?= $row->qty; ?>" required>
  	                      <small class="text-danger">
  	                        <?php echo form_error('qty') ?>
  	                      </small>
  	                    </div>
  	                  </div>
  	                  <div class="form-group row">
  	                    <label class="col-md-3 col-form-label">Harga</label>
  	                    <div class="col-md-9">
  	                      <input class="form-control" type="number" name="harga" placeholder="Isi Harga" value="<?= $row->harga; ?>" required>
  	                      <small class="text-danger">
  	                        <?php echo form_error('harga') ?>
  	                      </small>
  	                    </div>
  	                  </div>
  	                  <div class="form-group row">
  	                    <label class="col-md-3 col-form-label">Tanggal Masuk</label>
  	                    <div class="col-md-9">
  	                      <input class="form-control" type="date" name="tgl_masuk" value="<?= $row->tgl_masuk; ?>" required>
  	                      <small class="text-danger">
  	                        <?php echo form_error('tgl_masuk') ?>
  	                      </small>
  	                    </div>
  	                  </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>
<script>
  $(document).ready(function () {
//change selectboxes to selectize mode to be searchable
   $("select").select2();
});
</script>