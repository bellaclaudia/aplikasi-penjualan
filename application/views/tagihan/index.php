<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Tagihan</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Tagihan
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarian', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Agen</label>
              <div class="col-md-4">
                  <select class="form-control" id="select_page" name="agen">
                    <option value="" selected disabled>Pilih Agen</option>
                    <?php foreach ($data_agen as $k) : ?>
                      <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                    <?php endforeach; ?>
                  </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Rute</label>
              <div class="col-md-4">
                  <select class="form-control" id="select_page" name="rute">
                    <option value="" selected disabled>Pilih Rute</option>
                    <?php foreach ($data_rute as $k) : ?>
                      <option value="<?= $k['alamat']; ?>"><?= $k['alamat']; ?></option>
                    <?php endforeach; ?>
                  </select>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>
            </form>
            <hr>
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>
          <div style="overflow-x:auto;">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th></th>
                  <th>Agen</th>
                  <th>No. Faktur</th>
                  <th>Area</th>
                  <th>Nota</th>
                  <th>Customer</th>
                  <th>Total</th>
                  <!-- <th>0 s/d 30</th>
                  <th>31 s/d 60</th>
                  <th>61 s/d 90</th>
                  <th>91 s/d 120</th>
                  <th>121 s/d 150</th>
                  <th>> 150</th>
                  <th>bg</th>
                  <th>Fak</th>
                  <th>Ret</th>
                  <th>Pay</th> -->
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_tagihan as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->agen ?></td>
                    <td><?= $row->no_faktur ?></td>
                    <td><?= $row->alamat ?></td>
                    <td><?= $row->nota ?></td>
                    <td><?= $row->customer ?></td>
                    <td><?= "Rp " . number_format($row->total,0,',','.'); ?></td>
                    <!-- <td><?= $row->pertama ?></td>
                    <td><?= $row->kedua ?></td>
                    <td><?= $row->ketiga ?></td>
                    <td><?= $row->keempat ?></td>
                    <td><?= $row->kelima ?></td>
                    <td><?= $row->keenam ?></td>
                    <td><?= $row->bg ?></td>
                    <td><?= $row->fak ?></td>
                    <td><?= $row->ret ?></td>
                    <td><?= $row->pay ?></td> -->
                    <td>
                      <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top">Edit</a>
                      <a href="<?php echo site_url('tagihan/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data dengan No. Faktur <?= $row->no_faktur; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data">Hapus</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddBarang', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
  	                  <div class="form-group row">
  	                    <label class="col-md-3 col-form-label">Agen</label>
  	                    <div class="col-md-9">
  	                      <select class="form-control" id="select_page" name="id_agen">
  	                        <option value="" selected disabled>Pilih Agen</option>
  	                        <?php foreach ($data_agen as $k) : ?>
  	                          <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
  	                        <?php endforeach; ?>
  	                      </select>
  	                      <small class="text-danger">
  	                        <?php echo form_error('id_agen') ?>
  	                      </small>
  	                    </div>
  	                  </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nota</label>
                        <div class="col-md-9">
                          <select class="form-control" id="select_page" name="nota">
                            <option value="" selected disabled>Pilih Data Penjualan</option>
                            <?php foreach ($data_penjualan as $k) : ?>
                              <option value="<?= $k['no_kwitansi']; ?>">No. Nota : <?= $k['no_kwitansi']; ?></option>
                            <?php endforeach; ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('nota') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">No. Faktur</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="no_faktur" placeholder="Isi No. Faktur" required>
                          <small class="text-danger">
                            <?php echo form_error('no_faktur') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Customer</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="customer" placeholder="Isi Customer" required>
                          <small class="text-danger">
                            <?php echo form_error('customer') ?>
                          </small>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                        <label class="col-md-3 col-form-label"> 0 s/d 30</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="pertama">
	                        <option value="" selected disabled>Pilih</option>
	                         <option value="Tidak">Tidak</option>
	                         <option value="Ya">Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('qty') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label"> 31 s/d 60</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="kedua">
	                        <option value="" selected disabled>Pilih</option>
	                         <option value="Tidak">Tidak</option>
	                         <option value="Ya">Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('kedua') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label"> 61 s/d 90</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="ketiga">
	                        <option value="" selected disabled>Pilih</option>
	                         <option value="Tidak">Tidak</option>
	                         <option value="Ya">Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('ketiga') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label"> 91 s/d 120</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="keempat">
	                        <option value="" selected disabled>Pilih</option>
	                         <option value="Tidak">Tidak</option>
	                         <option value="Ya">Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('keempat') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label"> 120 s/d 150</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="kelima">
	                        <option value="" selected disabled>Pilih</option>
	                         <option value="Tidak">Tidak</option>
	                         <option value="Ya">Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('kelima') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label"> >150</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="keenam">
	                        <option value="" selected disabled>Pilih</option>
	                         <option value="Tidak">Tidak</option>
	                         <option value="Ya">Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('keenam') ?>
                          </small>
                        </div>
                      </div> -->
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Jumlah</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="jumlah" placeholder="Isi Jumlah" required>
                          <small class="text-danger">
                            <?php echo form_error('jumlah') ?>
                          </small>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                        <label class="col-md-3 col-form-label">bg</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="bg" placeholder="Isi bg" required>
                          <small class="text-danger">
                            <?php echo form_error('bg') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">total</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="total" placeholder="Isi total" required>
                          <small class="text-danger">
                            <?php echo form_error('total') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Fak</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="fak" placeholder="Fak" required>
                          <small class="text-danger">
                            <?php echo form_error('fak') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Ret</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="ret" placeholder="Isi Ret" required>
                          <small class="text-danger">
                            <?php echo form_error('ret') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Pay</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="pay" placeholder="Isi Pay" required>
                          <small class="text-danger">
                            <?php echo form_error('pay') ?>
                          </small>
                        </div>
                      </div> -->
	                  <div class="modal-footer">
	                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
	                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
	                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_tagihan as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="FrmAdd" class="form-horizontal" action="<?php echo site_url('tagihan/edit/'.$row->id); ?>" method="post" enctype="multipart/form-data" autocomplete="off">
                      <div class="form-group row">
	                    <label class="col-md-3 col-form-label">Agen</label>
	                    <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
	                      <select class="form-control" id="select_page" name="id_agen">
	                        <option value="" selected disabled>Pilih Agen</option>
	                         <?php
                              $lv = '';
                              if (isset($row->id_agen)) {
                                $lv = $row->id_agen;
                              }
                              foreach ($data_agen as $r => $v) {
                              ?>
                                <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama']; ?></option>
                              <?php
                              }
                            ?>
	                      </select>
	                      <small class="text-danger">
	                        <?php echo form_error('id_agen') ?>
	                      </small>
	                    </div>
	                  </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nota</label>
                        <div class="col-md-9">
                          <select class="form-control" id="select_page" name="nota">
                            <?php
                              $lv = '';
                              if (isset($row->nota)) {
                                $lv = $row->nota;
                              }
                              foreach ($data_penjualan as $r => $v) {
                              ?>
                                <option value="<?= $k['no_kwitansi']; ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>>No. Nota : <?= $v['no_kwitansi']; ?></option>
                              <?php
                              }
                            ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('nota') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">No. Faktur</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="no_faktur" placeholder="Isi No. Faktur" required value="<?= $row->no_faktur; ?>">
                          <small class="text-danger">
                            <?php echo form_error('no_faktur') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Customer</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="customer" placeholder="Isi Customer" required value="<?= $row->customer; ?>">
                          <small class="text-danger">
                            <?php echo form_error('customer') ?>
                          </small>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                        <label class="col-md-3 col-form-label"> 0 s/d 30</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="pertama">
                            <option value="0" <?= $row->pertama == 0 ? 'selected' : '' ?>>Pilih</option>
                            <option value="Tidak" <?= $row->pertama == 'Tidak' ? 'selected' : '' ?>>Tidak</option>
                            <option value="Ya" <?= $row->pertama == 'Ya' ? 'selected' : '' ?>>Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('qty') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label"> 31 s/d 60</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="kedua">
	                        <option value="0" <?= $row->kedua == 0 ? 'selected' : '' ?>>Pilih</option>
                            <option value="Tidak" <?= $row->kedua == 'Tidak' ? 'selected' : '' ?>>Tidak</option>
                            <option value="Ya" <?= $row->kedua == 'Ya' ? 'selected' : '' ?>>Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('kedua') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label"> 61 s/d 90</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="ketiga">
	                        <option value="0" <?= $row->ketiga == 0 ? 'selected' : '' ?>>Pilih</option>
                            <option value="Tidak" <?= $row->ketiga == 'Tidak' ? 'selected' : '' ?>>Tidak</option>
                            <option value="Ya" <?= $row->ketiga == 'Ya' ? 'selected' : '' ?>>Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('ketiga') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label"> 91 s/d 120</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="keempat">
	                        <option value="0" <?= $row->keempat == 0 ? 'selected' : '' ?>>Pilih</option>
                            <option value="Tidak" <?= $row->keempat == 'Tidak' ? 'selected' : '' ?>>Tidak</option>
                            <option value="Ya" <?= $row->keempat == 'Ya' ? 'selected' : '' ?>>Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('keempat') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label"> 120 s/d 150</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="kelima">
	                        <option value="0" <?= $row->kelima == 0 ? 'selected' : '' ?>>Pilih</option>
                            <option value="Tidak" <?= $row->kelima == 'Tidak' ? 'selected' : '' ?>>Tidak</option>
                            <option value="Ya" <?= $row->kelima == 'Ya' ? 'selected' : '' ?>>Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('kelima') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label"> >150</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" name="keenam">
	                        <option value="0" <?= $row->keenam == 0 ? 'selected' : '' ?>>Pilih</option>
                            <option value="Tidak" <?= $row->keenam == 'Tidak' ? 'selected' : '' ?>>Tidak</option>
                            <option value="Ya" <?= $row->keenam == 'Ya' ? 'selected' : '' ?>>Ya</option>
	                      </select>
                          <small class="text-danger">
                            <?php echo form_error('keenam') ?>
                          </small>
                        </div>
                      </div> -->
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Jumlah</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="jumlah" placeholder="Isi Jumlah" required value="<?= $row->jumlah; ?>">
                          <small class="text-danger">
                            <?php echo form_error('jumlah') ?>
                          </small>
                        </div>
                      </div>
                      <!-- <div class="form-group row">
                        <label class="col-md-3 col-form-label">bg</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="bg" placeholder="Isi bg" required value="<?= $row->bg; ?>">
                          <small class="text-danger">
                            <?php echo form_error('bg') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">total</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="total" placeholder="Isi total" required value="<?= $row->total; ?>">
                          <small class="text-danger">
                            <?php echo form_error('total') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Fak</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="fak" placeholder="Fak" required value="<?= $row->fak; ?>">
                          <small class="text-danger">
                            <?php echo form_error('fak') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Ret</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="ret" placeholder="Isi Ret" required value="<?= $row->ret; ?>">
                          <small class="text-danger">
                            <?php echo form_error('ret') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Pay</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="pay" placeholder="Isi Pay" required value="<?= $row->pay; ?>">
                          <small class="text-danger">
                            <?php echo form_error('pay') ?>
                          </small>
                        </div>
                      </div> -->
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>
<script>
  $(document).ready(function () {
//change selectboxes to selectize mode to be searchable
   $("select").select2();
});
</script>