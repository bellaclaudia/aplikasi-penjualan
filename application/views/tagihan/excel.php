<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=faktur_retur.xls");
header("Cache-control: public");
?>
<style>
    .allcen{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .allcen2{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .str{ 
        mso-number-format:\@; 
    }
</style>
<head>
    <meta charset="utf-8" />
    <title>AJG | Tagihan</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <style>
        body {
            font-family: verdana,arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
        }
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size:11px;
            width: 100%;
            color:#333333;
            border-width: 1px;
            border-color: #e9e9e9;
            border-collapse: collapse;
        }
        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            font-size:12px;
            border-style: solid;
            font-weight: 900;
            color: #ffffff;
            border-color: #e9e9e9;
            background: #ea6153;
        }
        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #e9e9e9;
            background-color: #ffffff;
        }

    </style>
</head>
<body>
<h4 style="text-align: center">Data Tagihan pada AJG</h4>
<table border="1" width="100%" class="gridtable">
  <thead style="background-color: blue">
    <tr>
      	<th class="allcen center bold">No.</th>
        <th class="allcen center bold">Agen</th>
        <th>No. Faktur</th>
          <th class="allcen center bold">Area</th>
          <th class="allcen center bold">Nota</th>
          <th class="allcen center bold">Customer</th>
          <th class="allcen center bold">Total</th>
          <!-- <th class="allcen center bold">0 s/d 30</th>
          <th class="allcen center bold">31 s/d 60</th>
          <th class="allcen center bold">61 s/d 90</th>
          <th class="allcen center bold">91 s/d 120</th>
          <th class="allcen center bold">121 s/d 150</th>
          <th class="allcen center bold">> 150</th>
          <th class="allcen center bold">bg</th>
          <th class="allcen center bold">total</th>
          <th class="allcen center bold">Fak</th>
          <th class="allcen center bold">Ret</th>
          <th class="allcen center bold">Pay</th> -->
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($data_faktur_retur as $row) : ?>
      <tr>
        <td><?= $i++; ?></td>
        <td><?= $row->agen ?></td>
        <td><?= $row->no_faktur ?></td>
        <td><?= $row->nama_rute ?></td>
        <td><?= $row->nota ?></td>
        <td><?= $row->customer ?></td>
        <td><?= "Rp " . number_format($row->total,0,',','.'); ?></td>
        <!-- <td><?= $row->pertama ?></td>
        <td><?= $row->kedua ?></td>
        <td><?= $row->ketiga ?></td>
        <td><?= $row->keempat ?></td>
        <td><?= $row->kelima ?></td>
        <td><?= $row->keenam ?></td>
        <td><?= $row->bg ?></td>
        <td><?= $row->total ?></td>
        <td><?= $row->fak ?></td>
        <td><?= $row->ret ?></td>
        <td><?= $row->pay ?></td> -->
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>