<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Faktur / Retur</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Faktur / Retur
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarian', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Agen</label>
              <div class="col-md-4">
                  <select class="form-control select2-single" id="select2-7" name="agen">
                    <option value="" selected disabled>Pilih Agen</option>
                    <?php foreach ($data_agen as $k) : ?>
                      <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                    <?php endforeach; ?>
                  </select>
                  <small class="text-danger">
                    <?php echo form_error('agen') ?>
                  </small>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>
            </form>
            <hr>
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>
          <div style="overflow-x:auto;">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th></th>
                  <th>Agen</th>
                  <th>Saldo Akhir</th>
                  <th>Harga</th>
                  <th>Qty</th>
                  <th>Jumlah</th>
                  <th>Transaksi</th>
                  <th>Nama Sales</th>
                  <th>No. Faktur</th>
                  <th>Nama Rute</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_faktur_retur as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->agen ?></td>
                    <td><?= $row->saldo_akhir ?></td>
                    <td>
                      <?php if ($row->harga != '') { ?>
                      <?= "Rp " . number_format($row->harga,2,',','.'); ?>
                      <?php } ?>
                    </td>
                    <td><?= $row->qty ?></td>
                    <td>
                      <?php if ($row->harga != '') { ?>
                      <?= "Rp " . number_format($row->harga*$row->qty,2,',','.'); ?>
                      <?php } ?>
                    </td>
                    <td><?= $row->transaksi ?></td>
                    <td><?= $row->nama_sales ?></td>
                    <td><?= $row->no_faktur ?></td>
                    <td><?= $row->nama_rute ?></td>
                    <td><?= $row->status ?></td>
                    <td>
                      <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top">Edit</a>
                      <a href="<?php echo site_url('faktur_retur/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data dengan No. Faktur <?= $row->no_faktur; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data">Hapus</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddBarang', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Agen</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-1" name="id_agen">
                        <option value="" selected disabled>Pilih Agen</option>
                        <?php foreach ($data_agen as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger">
                        <?php echo form_error('id_agen') ?>
                      </small>
                    </div>
                  </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Saldo Akhir</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="saldo_akhir" placeholder="Saldo Akhir"required>
                          <small class="text-danger">
                            <?php echo form_error('saldo_akhir') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Harga</label>
                        <div class="col-md-9">
                          <input class="form-control" type="number" name="harga" placeholder="Isi Harga" required>
                          <small class="text-danger">
                            <?php echo form_error('harga') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">QTY</label>
                        <div class="col-md-9">
                          <input class="form-control" type="number" name="qty" placeholder="QTY" required>
                          <small class="text-danger">
                            <?php echo form_error('qty') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Transaksi</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-2" name="transaksi" required>
                            <option value="" selected disabled>Pilih Transaksi</option>
                            <option value="FAKTUR">FAKTUR</option>
                            <option value="RETUR">RETUR</option>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('transaksi') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nama Sales</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="nama_sales" placeholder="Nama Sales" required>
                          <small class="text-danger">
                            <?php echo form_error('nama_sales') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">No. Faktur</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="no_faktur" placeholder="No. Faktur" required>
                          <small class="text-danger">
                            <?php echo form_error('no_faktur') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nama Rute</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="nama_rute" placeholder="Nama Rute" required>
                          <small class="text-danger">
                            <?php echo form_error('nama_rute') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Status</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-3" name="status" required>
                            <option value="" selected disabled>Pilih Status</option>
                            <option value="Fresh">Fresh</option>
                            <option value="TH">TH</option>
                            <option value="Jablak">Jablak</option>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('status') ?>
                          </small>
                        </div>
                      </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_faktur_retur as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="FrmAdd" class="form-horizontal" action="<?php echo site_url('faktur_retur/edit/'.$row->id); ?>" method="post" enctype="multipart/form-data" autocomplete="off">
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Agen</label>
                        <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <select class="form-control select2-single" id="select2-4" name="id_agen">
                            <option value="" selected disabled>Pilih Agen</option>
                            <?php
                              $lv = '';
                              if (isset($row->id_agen)) {
                                $lv = $row->id_agen;
                              }
                              foreach ($data_agen as $r => $v) {
                              ?>
                                <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
                              <?php
                              }
                              ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('id_agen') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Saldo Akhir</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="saldo_akhir" placeholder="Saldo Akhir" value="<?= $row->saldo_akhir; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('saldo_akhir') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Harga</label>
                        <div class="col-md-9">
                          <input class="form-control" type="number" name="harga" placeholder="Isi Harga" value="<?= $row->harga; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('harga') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">QTY</label>
                        <div class="col-md-9">
                          <input class="form-control" type="number" name="qty" placeholder="QTY" value="<?= $row->qty; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('qty') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Transaksi</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-5" name="transaksi" required>
                            <option value="0" <?= $row->transaksi == 0 ? 'selected' : '' ?>>Pilih Transaksi</option>
                            <option value="FAKTUR" <?= $row->transaksi == 'FAKTUR' ? 'selected' : '' ?>>FAKTUR</option>
                            <option value="RETUR" <?= $row->transaksi == 'RETUR' ? 'selected' : '' ?>>RETUR</option>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('transaksi') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nama Sales</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="nama_sales" placeholder="Nama Sales" value="<?= $row->nama_sales; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('nama_sales') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">No. Faktur</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="no_faktur" placeholder="No. Faktur" value="<?= $row->no_faktur; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('no_faktur') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nama Rute</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="nama_rute" placeholder="Nama Rute" value="<?= $row->nama_rute; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('nama_rute') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Status</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-6" name="status" required>
                            <option value="0" <?= $row->status == 0 ? 'selected' : '' ?>>Pilih Status</option>
                            <option value="Fresh" <?= $row->status == 'Fresh' ? 'selected' : '' ?>>Fresh</option>
                            <option value="TH" <?= $row->status == 'TH' ? 'selected' : '' ?>>TH</option>
                            <option value="Jablak" <?= $row->status == 'Jablak' ? 'selected' : '' ?>>Jablak</option>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('status') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>