<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=faktur_retur.xls");
header("Cache-control: public");
?>
<style>
    .allcen{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .allcen2{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .str{ 
        mso-number-format:\@; 
    }
</style>
<head>
    <meta charset="utf-8" />
    <title>AJG | Faktur/Retur</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <style>
        body {
            font-family: verdana,arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
        }
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size:11px;
            width: 100%;
            color:#333333;
            border-width: 1px;
            border-color: #e9e9e9;
            border-collapse: collapse;
        }
        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            font-size:12px;
            border-style: solid;
            font-weight: 900;
            color: #ffffff;
            border-color: #e9e9e9;
            background: #ea6153;
        }
        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #e9e9e9;
            background-color: #ffffff;
        }

    </style>
</head>
<body>
<h4 style="text-align: center">Faktur / Retur pada AJG</h4>
<table border="1" width="100%" class="gridtable">
  <thead style="background-color: blue">
    <tr>
      	<th class="allcen center bold">No.</th>
        <th class="allcen center bold">Agen</th>
        <th class="allcen center bold">Saldo Akhir</th>
        <th class="allcen center bold">Harga</th>
        <th class="allcen center bold">Qty</th>
        <th class="allcen center bold">Jumlah</th>
        <th class="allcen center bold">Transaksi</th>
        <th class="allcen center bold">Nama Sales</th>
        <th class="allcen center bold">No. Faktur</th>
        <th class="allcen center bold">Nama Rute</th>
        <th class="allcen center bold">Status</th>
        <th class="allcen center bold">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($data_faktur_retur as $row) : ?>
      <tr>
        <td><?= $i++; ?></td>
        <td><?= $row->agen ?></td>
        <td><?= $row->saldo_akhir ?></td>
        <td>
          <?php if ($row->harga != '') { ?>
          <?= "Rp " . number_format($row->harga,2,',','.'); ?>
          <?php } ?>
        </td>
        <td><?= $row->qty ?></td>
        <td>
          <?php if ($row->harga != '') { ?>
          <?= "Rp " . number_format($row->harga*$row->qty,2,',','.'); ?>
          <?php } ?>
        </td>
        <td><?= $row->transaksi ?></td>
        <td><?= $row->nama_sales ?></td>
        <td><?= $row->no_faktur ?></td>
        <td><?= $row->nama_rute ?></td>
        <td><?= $row->status ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>