<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data agen</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data agen
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>
          <div style="overflow-x:auto;">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th></th>
                  <th>Nama Agen</th>
                  <th>Alamat</th>
                  <th>Bank</th>
                  <th>Atas Nama</th>
                  <th>No. Rekening</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_agen as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->nama ?></td>
                    <td><?= $row->alamat ?></td>
                    <td><?= $row->bank ?></td>
                    <td><?= $row->atas_nama ?></td>
                    <td><?= $row->no_rek ?></td>
                    <td>
                      <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top">Edit</a>
                      <a href="<?php echo site_url('agen/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Agen <?= $row->nama; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data">Hapus</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddBarang', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Nama agen</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="nama" placeholder="Nama agen" required>
                      <small class="text-danger">
                        <?php echo form_error('nama') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Alamat</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="alamat" placeholder="Alamat" required>
                      <small class="text-danger">
                        <?php echo form_error('alamat') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Bank</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="bank" placeholder="Isi Bank" required>
                      <small class="text-danger">
                        <?php echo form_error('bank') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Atas Nama</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="atas_nama" placeholder="Atas Nama" required>
                      <small class="text-danger">
                        <?php echo form_error('atas_nama') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">No. Rekening</label>
                    <div class="col-md-9">
                      <input class="form-control" type="number" name="no_rek" placeholder="No. Rekening" required>
                      <small class="text-danger">
                        <?php echo form_error('no_rek') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_agen as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="FrmAdd" class="form-horizontal" action="<?php echo site_url('agen/edit/'.$row->id); ?>" method="post" enctype="multipart/form-data" autocomplete="off">
	  	              <div class="form-group row">
	                    <label class="col-md-3 col-form-label">Nama agen</label>
	                    <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
	                      <input class="form-control" type="text" name="nama" placeholder="Nama agen" value="<?= $row->nama; ?>" required>
	                      <small class="text-danger">
	                        <?php echo form_error('nama') ?>
	                      </small>
	                    </div>
	                  </div>
	                  <div class="form-group row">
	                    <label class="col-md-3 col-form-label">Alamat</label>
	                    <div class="col-md-9">
	                      <input class="form-control" type="text" name="alamat" placeholder="Alamat" value="<?= $row->alamat; ?>" required>
	                      <small class="text-danger">
	                        <?php echo form_error('alamat') ?>
	                      </small>
	                    </div>
	                  </div>
	                  <div class="form-group row">
	                    <label class="col-md-3 col-form-label">Bank</label>
	                    <div class="col-md-9">
	                      <input class="form-control" type="text" name="bank" placeholder="Isi Bank" value="<?= $row->bank; ?>" required>
	                      <small class="text-danger">
	                        <?php echo form_error('bank') ?>
	                      </small>
	                    </div>
	                  </div>
	                  <div class="form-group row">
	                    <label class="col-md-3 col-form-label">Atas Nama</label>
	                    <div class="col-md-9">
	                      <input class="form-control" type="text" name="atas_nama" placeholder="Atas Nama" value="<?= $row->atas_nama; ?>" required>
	                      <small class="text-danger">
	                        <?php echo form_error('atas_nama') ?>
	                      </small>
	                    </div>
	                  </div>
	                  <div class="form-group row">
	                    <label class="col-md-3 col-form-label">No. Rekening</label>
	                    <div class="col-md-9">
	                      <input class="form-control" type="number" name="no_rek" placeholder="No. Rekening" value="<?= $row->no_rek; ?>" required>
	                      <small class="text-danger">
	                        <?php echo form_error('no_rek') ?>
	                      </small>
	                    </div>
	                  </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>