<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=stok.xls");
header("Cache-control: public");
?>
<style>
    .allcen{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .allcen2{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .str{ 
        mso-number-format:\@; 
    }
</style>
<head>
    <meta charset="utf-8" />
    <title>AJG | DATA BARANG</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <style>
        body {
            font-family: verdana,arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
        }
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size:11px;
            width: 100%;
            color:#333333;
            border-width: 1px;
            border-color: #e9e9e9;
            border-collapse: collapse;
        }
        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            font-size:12px;
            border-style: solid;
            font-weight: 900;
            color: #ffffff;
            border-color: #e9e9e9;
            background: #ea6153;
        }
        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #e9e9e9;
            background-color: #ffffff;
        }

    </style>
</head>
<body>
<h4 style="text-align: center">Data Barang pada AJG</h4>
<table border="1" width="100%" class="gridtable">
  <thead style="background-color: blue">
    <tr>
      	<th class="allcen center bold">No.</th>
        <th class="allcen center bold">Agen</th>
      	<th class="allcen center bold">Nama Barang</th>
      	<th class="allcen center bold">Stok</th>
      	<th class="allcen center bold">Harga</th>
        <th class="allcen center bold">Total</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($data_barang as $row) : ?>
      <tr>
        <td><?= $i++; ?></td>
        <td><?= $row->agen ?></td>
        <td><?= $row->nama ?></td>
        <td><?= $row->stok ?></td>
        <td><?= "Rp " . number_format($row->harga,2,',','.'); ?></td>
        <td><?= "Rp " . number_format($row->harga*$row->stok,2,',','.'); ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>