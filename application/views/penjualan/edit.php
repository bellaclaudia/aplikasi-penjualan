<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('dosen'); ?>">Data Penjualan</a></li>
    <li class="breadcrumb-item active">Edit Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Edit Data
        </div>
        <div class="card-body">
          <form id="FrmAdd" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
          	<?php 
            $i=1;
            $databarang=explode(', ', $data_penjualan['id_barang']);
            foreach ($data_barang as $k) { ?>

          <div class="form-group row">
              <label class="col-md-3 col-form-label">Nama Barang</label>
              <div class="col-md-9">
              	<input type="hidden" name="id[<?php echo $i; ?>]" value="<?= $data_penjualan['id'] ?>">
                <input type="checkbox" name="id_barang[<?php echo $i; ?>]" value="<?= $k['id']; ?>" <?php if (in_array($k['id'], $databarang)) echo "checked";?>> <b><?= $k['nama']; ?></b>
              </div>
            </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">QTY</label>
            <div class="col-md-9"> 
              <input class="form-control" type="number" name="qty[<?php echo $i; ?>]" value="<?= $data_penjualan['qty'] ?> <?php if (in_array($k['qty'], $databarang)) echo "checked";?>" placeholder="QTY">
            </div>
          </div>
          <?php $i++;} ?>
            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('dosen'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>