<!DOCTYPE html>
<html>
<head>
    <title>Invoice</title>
</head>
<style type="text/css">
    body {
        font-family: "Arial";
        font-size: 8px;
        padding: 1%;
        padding-top: 5px;
        padding-bottom: 0;
        size: A5;
    }
    table {
      border-collapse: collapse;
    }
    td, th {
      border: 2px solid black;
    }
    .no-border td {
      border: 1px solid white !important;
    }
    th {
        padding: 5px;
    }
    td {
        padding-left: 5px;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    h2 {
        letter-spacing: 5px;
    }
    .bold {
        font-weight: 700;
    }
    .center {
        padding-left: 0;
        text-align: center;
    }
    .mb5 {
        margin-bottom: 5px;
    }
    .m0 {
        margin: 0;
    }
    .mt0 {
        margin-top: 0;
    }
    .mb10 {
        margin-bottom: 10px;
    }
    .ml10 {
        margin-left: 10px;
    }
    .mb20 {
        margin-bottom: 20px;
    }
    .mr20 {
        margin-right: 20px;
    }
    .mr50 {
        margin-right: 50px;
    }
</style>
<body>
    <table class="no-border" border="1" width="100%">
        <tr>
            <td width="30%">
                <table class="no-border" border="1">
                    <tr>
                        <td><b><?= $invoice['agen'] ?></b></td>
                    </tr>
                    <tr>
                        <td><b><?= $invoice['alamat_agen'] ?></b></td>
                    </tr>
                    <tr>
                        <td><b>Nama Sales : <?= $invoice['nama_sales'] ?></b></td>
                    </tr>
                    <tr>
                        <td><b>No. Nota : <?= $invoice['no_kwitansi'] ?></b></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="no-border" border="1">
                    <tr>
                        <td><b>Tanggal</b></td>
                        <td ><b> : <?= date('d-m-Y', strtotime($invoice['tgl_pembelian']))?></b></td>
                    </tr>
                    <tr>
                        <td><b>Kepada</b></td>
                        <td ><b> : <?= $invoice['nama_pembeli'] ?></b></td>
                    </tr>
                    <tr>
                        <td><b>Alamat</b></td>
                        <td ><b> : <?= $invoice['alamat'] ?></b></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table width="100%" border="1">
        <thead>
            <tr>
                <th width="3%">NO</th>
                <th width="25%">ARTIKEL</th>
                <th width="25%">NAMA BARANG</th>
                <th width="20%">HARGA</th>
                <th width="8%">QTY</th>
                <th width="20%">TOTAL</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>
            <?php 
                $jumlah = 0;
                $qty = 0;
                foreach ($data_invoice as $row) {
                $qty += $row['qty'];
                $jumlah += $row['jumlah'];
            ?>
            <tr>
                <td class="center"><?= $i++; ?></td>
                <td><?= $row['artikel'] ?></td>
                <td class="center"><?= $row['nama_barang'] ?></td>
                <td><?= "Rp " . number_format($row['harga'],0,',','.'); ?></td>
                <td class="center"><?= $row['qty'] ?></td>
                <td><?= "Rp " . number_format($row['qty']*$row['harga'],0,',','.'); ?></td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="3"></td>
                <td>TOTAL</td>
                <td class="center"><?= $qty ?></td>
                <td><?= "Rp " . number_format($jumlah,0,',','.'); ?></td>
            </tr>
        </tbody>
    </table>

    <table class="no-border" border="1" width="100%">
        <tr>
            <td width="70%">
                <p class="center bold">
                    PEMBAYARAN TRANSFER / GIRO
                    <br>A/n. <?= $invoice['atas_nama'] ?>
                    <br><?= $invoice['bank'] ?> : <?= $invoice['no_rek'] ?>
                    
                </p>
            </td>
            <td>
                <p class="center bold">
                    HORMAT KAMI, <?= date('d M Y') ?>
                    <br><br><br><br><br><br>
                    <?= $invoice['agen'] ?>
                </p>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        window.print();
    </script>
</body>
</html>