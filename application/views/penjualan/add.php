<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
    var kom=1;
    function hapus_kom(a) {
        //if (kom!=1){
            $('#row'+a).remove();
        //}
    }
    function lembaga() {

        $('.lembaga').select2({
            placeholder: 'Pencarian Nama Barang',
            theme: "bootstrap",
            ajax: {
                type: "POST",
                url: '<?php echo site_url('penjualan/pilihpegawai');?>',
                dataType: 'json',
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    }
    function tambah_kom() {
        kom=kom+1;
        s='<tr id="row'+kom+'">' +
        '<td> <select class="lembaga form-control" title="Lembaga Assesment" name="id_barang[]" style="width: 100%;" required="true"></select></td>' +
        '<td><input type="number" min="0" step=".01" class="form-control" name="qty[]" value="" required/></td>' +
        '<td style="vertical-align: middle">' +
        '<div class="text-center " style="vertical-align: middle">' +
        '<button type="button" class="text-center btn btn-xs btn-danger fa fa-remove" onclick="hapus_kom('+kom+')" title="Hapus"></button>' +
        '</div>' +
        '</td>' +
        '</tr>';
        $('#form_kom').append(s);
        lembaga();
    }
</script>
<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('dosen'); ?>">Data Dosen</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <div class="panel-body">
            <form action="<?= base_url('penjualan/add'); ?>" id="FrmAdd" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
              <div class="form-horizontal after-add-more">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label">Nama Sales</label>
                  <div class="col-md-2">
                    <input type="text" name="nama_sales" placeholder="Nama Sales" class="form-control" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label">Nama Pembeli</label>
                  <div class="col-md-2">
                    <input type="text" name="nama_pembeli" placeholder="Nama Pembeli" class="form-control" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 col-form-label">Alamat</label>
                  <div class="col-md-2">
                    <input type="text" name="alamat" placeholder="Alamat" class="form-control">
                  </div>
                </div><hr>
                <div class="col-md-12 ">
                  <button type="button" class="btn btn-sm btn btn-primary btn-xs fa fa-plus-circle" onclick="tambah_kom()"> Tambah </button><br><br>
                  <div style="overflow-x:auto;">
                    <table class="table table-bordered">
                      <thead>
                      <tr>
                          <th width="70%">Nama Barang</th>
                          <th width="20%">QTY</th>
                          <th width="10%">Hapus</th>
                      </tr>
                      </thead>
                      <tbody id="form_kom">
                      <tr id="row1">
                          <!-- <td>
                            <select class="lembaga form-control" title="Lembaga Assesment" name="id_barang[]" style="width: 100%;" required="true"></select>
                          </td>
                          <td><input type="number" min="0" class="form-control" name="qty[]" value="" step=".01" required/></td>
                          <td style="vertical-align: middle">
                              <div class="text-center " style="vertical-align: middle">
                                  <button type="button" class="text-center btn btn-xs btn-danger fa fa-remove" onclick="hapus_kom(1)" title="Hapus"></button>
                              </div>
                          </td> -->
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button class="btn btn-sm btn-primary btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
                <a href="<?= base_url('penjualan'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>
<script>
    $(function () {
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        $(".tanggal").datepicker( {

            format: 'yyyy-mm-dd'
        });
        $(".tahun").datepicker( {

            format: 'yyyy',
            viewMode: "years",
            minViewMode: "years"
        });
    });

    $('.lembaga').select2({
            placeholder: 'Pencarian Nama Barang',
            theme: "bootstrap",
            ajax: {
                type: "POST",
                url: '<?php echo site_url('penjualan/pilihpegawai');?>',
                dataType: 'json',
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
</script>