<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Penjualan</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Penjualan
        </div>
        <div class="card-body">
        	<?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarian', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tanggal Awal</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_awal">
                <small class="text-danger">
                  <?php echo form_error('tgl_awal') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tanggal Akhir</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_akhir">
                <small class="text-danger">
                  <?php echo form_error('tgl_akhir') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Agen</label>
              <div class="col-md-4">
                  <select class="form-control select2-single" id="select2-3" name="agen">
                    <option value="" selected disabled>Pilih Agen</option>
                    <?php foreach ($data_agen as $k) : ?>
                      <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                    <?php endforeach; ?>
                  </select>
                  <small class="text-danger">
                    <?php echo form_error('agen') ?>
                  </small>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>
            </form>
            <hr>
          <?php foreach ($data_agen as $k) : ?>
          <a href="<?= base_url('penjualan/tambah/'. $k['id']); ?>"><button class="btn btn-sm btn-success mb-1" type="button">Tambah Data <?= $k['nama']; ?></button></a><?php endforeach; ?><br><br>
          <div style="overflow-x:auto;">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th></th>
                  <th>Nama Sales</th>
                  <th>Nama Pembeli</th>
                  <th>Alamat</th>
                  <th>Agen</th>
                  <th>No. Kwitansi</th>
                  <th>QTY</th>
                  <th>Jumlah</th>
                  <th>Tanggal Pembelian</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $i = 1; 
                ?>
                <?php 
                $qty = 0;
                $jumlah = 0;
                foreach ($data_penjualan as $row) {
                  if($qty > 0 or $jumlah > 0) {
                $qty += $row->qty;
                $jumlah += $row->jumlah; }
                ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->nama_sales ?></td>
                    <td><?= $row->nama_pembeli ?></td>
                    <td><?= $row->alamat ?></td>
                    <td><?= $row->agen ?></td>
                    <td><?= $row->no_kwitansi ?></td>
                    <td><?= $row->jml_qty ?></td>
                    <td><?= "Rp " . number_format($row->total,0,',','.'); ?></td>
                    <td><?= date('d-m-Y', strtotime($row->tgl_pembelian))?></td>
                    <td>
                      <!-- <a href="<?= base_url('penjualan/edit/' . $row->no_kwitansi); ?>" class="btn btn-success btn-circle">Edit</a> -->
                      <a target="_blank" href="<?= base_url('penjualan/pdf/' . $row->no_kwitansi); ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Invoice" data-popup="tooltip" data-placement="top">Nota</a>
                      <a href="<?php echo site_url('penjualan/hapus/' . $row->no_kwitansi); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Penjualan dengan No. Kwitansi <?= $row->no_kwitansi; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data">Hapus</a>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>