<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('dosen'); ?>">Data Dosen</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <form action="<?= base_url('penjualan/add'); ?>" id="FrmAdd" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <!-- <button type="button" id="btn-tambah-form" class="btn btn-sm btn-primary btn-ladda">Tambah Data Form</button>
            <button type="button" id="btn-reset-form" class="btn btn-sm btn-danger btn-ladda">Reset Form</button><br><br> -->
            <div class="form-group row">
              <label class="col-md-3 col-form-label">Nama Pembeli</label>
              <div class="col-md-2">
                <input type="text" name="nama_pembeli" placeholder="Nama Pembeli" class="form-control" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label">Alamat</label>
              <div class="col-md-2">
                <input type="text" name="alamat" placeholder="Alamat" class="form-control">
              </div>
            </div><hr>
          	<?php 
            $i=1;
            foreach ($data_barang as $k) { ?>
            <!-- <b>Data ke 1 :</b> -->
            <div class="form-group row">
              <label class="col-md-3 col-form-label">Barang</label>
              <div class="col-md-9">
                <select class="form-control multiple-single" multiple id="select2-1" name="id_barang[<?php echo $k['id']; ?>]">
                  <option value="" selected disabled>Pilih Barang</option>
                  <?php foreach ($data_barang as $k) : ?>
                    <option value="<?= $k['id']; ?>">Kode Barang : <?= $k['kd_barang']; ?> | Nama Barang : <?= $k['artikel']; ?> | Artikel : <?= $k['artikel']; ?> | Stok : <?= $k['stok']; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label">QTY</label>
              <div class="col-md-2">
                <input class="form-control" type="number" name="qty[<?php echo $k['id']; ?>]" placeholder="QTY">
              </div>
            </div><hr>
            <!-- <div id="insert-form"></div> -->
            <?php $i++;} ?>
            
            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('penjualan'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form><!-- 
          <input type="hidden" id="jumlah-form" value="1"> -->
        </div>
      </div>
    </div>
  </div>
</main>
</div>
<script>
  $(document).ready(function(){ // Ketika halaman sudah diload dan siap
    $("#btn-tambah-form").click(function(){ // Ketika tombol Tambah Data Form di klik
      var jumlah = parseInt($("#jumlah-form").val()); // Ambil jumlah data form pada textbox jumlah-form
      var nextform = jumlah + 1; // Tambah 1 untuk jumlah form nya
      // Kita akan menambahkan form dengan menggunakan append
      // pada sebuah tag div yg kita beri id insert-form
      $("#insert-form").append("<b>Data ke " + nextform + " :</b>" +
        "<div class='form-group row'>" +
        "<label class='col-md-3 col-form-label'>Barang</label>" +
        "<div class='col-md-9'>" +
        "<select class='form-control select2' name='id_barang[<?php echo $k['id']; ?>]'>" +
        "<option value='' selected disabled>Pilih Barang</option>" +
        "<?php foreach ($data_barang as $k) : ?>" +
        "<option value='<?= $k['id']; ?>'>Kode Barang : <?= $k['kd_barang']; ?> | Nama Barang : <?= $k['artikel']; ?> | Artikel : <?= $k['artikel']; ?> | Stok : <?= $k['stok']; ?></option>" +
        "<?php endforeach; ?>" +
        "</select>" +
        "</div>" +
        "</div>" +
        "<br>");
      
      $("#jumlah-form").val(nextform); // Ubah value textbox jumlah-form dengan variabel nextform
    });
    
    // Buat fungsi untuk mereset form ke semula
    $("#btn-reset-form").click(function(){
      $("#insert-form").html(""); // Kita kosongkan isi dari div insert-form
      $("#jumlah-form").val("1"); // Ubah kembali value jumlah form menjadi 1
    });
  });
  </script>