<div class="app-body">
  <div class="sidebar">
    <nav class="sidebar-nav">
      <ul class="nav">
        <li class="nav-title">Menu</li>
        <?php if (isset($this->session->userdata['logged_in'])) { // jika udh lgin 
        ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
              <i class="nav-icon icon-speedometer"></i> Dashboard
            </a>
          </li>
          <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="nav-icon fa fa-share-alt"></i> Pengaturan</a>
            <ul class="nav-dropdown-items">
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url('master_user'); ?>">
                  <i class="nav-icon fa fa-user"></i> Data User
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url('ganti_pass') ?>">
                  <i class="nav-icon fa fa-users"></i> Ganti Password
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('agen'); ?>">
              <i class="nav-icon icon-list"></i> Data Agen
            </a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="<?= base_url('artikel'); ?>">
              <i class="nav-icon icon-list"></i> Data Artikel
            </a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('barang'); ?>">
              <i class="nav-icon icon-list"></i> Data Barang
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('stok'); ?>">
              <i class="nav-icon icon-list"></i> Data Stok
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('penjualan'); ?>">
              <i class="nav-icon icon-list"></i> Data Penjualan
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('faktur_retur'); ?>">
              <i class="nav-icon icon-list"></i> Faktur/Retur
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('tagihan'); ?>">
              <i class="nav-icon icon-list"></i> Tagihan
            </a>
          </li>
        <?php }
        ?>
      </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
  </div>