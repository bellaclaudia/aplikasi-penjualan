<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stok extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("stok_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $stok = $this->stok_m;
        $validation = $this->form_validation;
        $validation->set_rules($stok->rules());
        if ($validation->run()) {
            $stok->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Stok berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("stok");
        }

        $agen = $this->input->post('agen');
        $excel = $this->input->post('excel');

        if ($agen == '') $agen = NULL;

        $data["data_agen"] = $this->stok_m->getAgen();
        $data["data_stok"] = $this->stok_m->getAll($agen);

        $data["agen"] = $agen;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('stok/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('stok/excel', $data);
    }

    public function print()
    {
        $data["data_stok"] = $this->stok_m->getAll();
        $this->load->view('stok/excel', $data);
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('id_agen', 'id_agen', 'required');
        $this->form_validation->set_rules('kd_barang', 'kd_barang', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('artikel', 'artikel', 'required');
        $this->form_validation->set_rules('harga', 'harga', 'required');
        $this->form_validation->set_rules('stok', 'stok', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Stok gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('stok');
        } else {
            $update = array(
                "id_agen" => $this->input->post('id_agen'),
                "kd_barang" => $this->input->post('kd_barang'),
                "nama" => $this->input->post('nama'),
                "artikel" => $this->input->post('artikel'),
                "harga" => $this->input->post('harga'),
                "stok" => $this->input->post('stok'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );

            $this->db->where('id', $id);
            $this->db->update('stok', $update);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Stok berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('stok');
        }

    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Stok gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('stok');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('stok');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Stok berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('stok');
        }
    }
}
