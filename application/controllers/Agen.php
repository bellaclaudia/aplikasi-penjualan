<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("agen_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $agen = $this->agen_m;
        $validation = $this->form_validation;
        $validation->set_rules($agen->rules());
        if ($validation->run()) {
            $agen->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Agen berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("agen");
        }

        $data["title"] = "agen";
        $data["data_agen"] = $this->agen_m->getAll();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('agen/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('alamat', 'alamat', 'required');
        $this->form_validation->set_rules('bank', 'bank', 'required');
        $this->form_validation->set_rules('atas_nama', 'atas nama', 'required');
        $this->form_validation->set_rules('no_rek', 'no_rek', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Agen gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('agen');
        } else {
            $update = array(
                "nama" => $this->input->post('nama'),
                "alamat" => $this->input->post('alamat'),
                "bank" => $this->input->post('bank'),
                "atas_nama" => $this->input->post('atas_nama'),
                "no_rek" => $this->input->post('no_rek'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );

            $this->db->where('id', $id);
            $this->db->update('agen', $update);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Agen berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('agen');
        }

    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Agen gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('agen');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('agen');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Agen berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('agen');
        }
    }
}
