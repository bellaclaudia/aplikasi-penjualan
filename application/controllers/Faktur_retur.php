<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Faktur_retur extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("faktur_retur_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $faktur_retur = $this->faktur_retur_m;
        $validation = $this->form_validation;
        $validation->set_rules($faktur_retur->rules());
        if ($validation->run()) {
            $faktur_retur->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Faktur / Retur berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("faktur_retur");
        }

        $agen = $this->input->post('agen');
        $excel = $this->input->post('excel');

        if ($agen == '') $agen = NULL;
        
        $data["data_agen"] = $this->faktur_retur_m->getAgen();
        $data["data_faktur_retur"] = $this->faktur_retur_m->getAll($agen);

        $data["agen"] = $agen;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('faktur_retur/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('faktur_retur/excel', $data);
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('id_agen', 'id_agen', 'required');
        $this->form_validation->set_rules('saldo_akhir', 'saldo_akhir', 'required');
        $this->form_validation->set_rules('harga', 'harga', 'required');
        $this->form_validation->set_rules('qty', 'qty', 'required');
        $this->form_validation->set_rules('transaksi', 'transaksi', 'required');
        $this->form_validation->set_rules('nama_sales', 'nama_sales', 'required');
        $this->form_validation->set_rules('no_faktur', 'no_faktur', 'required');
        $this->form_validation->set_rules('nama_rute', 'nama_rute', 'required');
        $this->form_validation->set_rules('status', 'status', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Faktur / Retur gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('faktur_retur');
        } else {
            $update = array(
                "id_agen" => $this->input->post('id_agen'),
	            "saldo_akhir" => $this->input->post('saldo_akhir'),
	            "harga" => $this->input->post('harga'),
	            "qty" => $this->input->post('qty'),
	            "transaksi" => $this->input->post('transaksi'),
	            "nama_sales" => $this->input->post('nama_sales'),
	            "no_faktur" => $this->input->post('no_faktur'),
	            "nama_rute" => $this->input->post('nama_rute'),
	            "status" => $this->input->post('status'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );

            $this->db->where('id', $id);
            $this->db->update('faktur_retur', $update);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Faktur / Retur berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('faktur_retur');
        }

    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Faktur / Retur gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('faktur_retur');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('faktur_retur');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Faktur / Retur berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('faktur_retur');
        }
    }
}
