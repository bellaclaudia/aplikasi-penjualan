<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tagihan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("tagihan_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $barang = $this->tagihan_m;
        $validation = $this->form_validation;
        $validation->set_rules($barang->rules());
        if ($validation->run()) {
            $barang->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Tagihan berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("tagihan");
        }

        $agen = $this->input->post('agen');
        $rute = $this->input->post('rute');
        $excel = $this->input->post('excel');

        if ($agen == '') $agen = NULL;
        if ($rute == '') $rute = NULL;
        
        $data["title"] = "tagihan";
        $data["data_agen"] = $this->tagihan_m->getAgen();
        $data["data_faktur"] = $this->tagihan_m->getFakturRetur();
        $data["data_penjualan"] = $this->tagihan_m->getPenjualan();
        $data["data_rute"] = $this->tagihan_m->getRute();
        $data["data_tagihan"] = $this->tagihan_m->getAll($agen,$rute);

        $data["agen"] = $agen;
        $data["rute"] = $rute;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('tagihan/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('tagihan/excel', $data);
    }

    public function print()
    {
        $data["data_tagihan"] = $this->tagihan_m->getAll();
        $this->load->view('tagihan/excel', $data);
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('id_faktur_retur', 'id_faktur_retur', 'required');
        $this->form_validation->set_rules('nota', 'nota', 'required');
        $this->form_validation->set_rules('customer', 'customer', 'required');
        // $this->form_validation->set_rules('pertama', 'pertama', 'required');
        // $this->form_validation->set_rules('kedua', 'kedua', 'required');
        // $this->form_validation->set_rules('ketiga', 'ketiga', 'required');
        // $this->form_validation->set_rules('keempat', 'keempat', 'required');
        // $this->form_validation->set_rules('kelima', 'kelima', 'required');
        // $this->form_validation->set_rules('keenam', 'keenam', 'required');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'required');
        // $this->form_validation->set_rules('bg', 'bg', 'required');
        // $this->form_validation->set_rules('total', 'total', 'required');
        // $this->form_validation->set_rules('fak', 'fak', 'required');
        // $this->form_validation->set_rules('ret', 'ret', 'required');
        // $this->form_validation->set_rules('pay', 'pay', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Tagihan gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('tagihan');
        } else {
            $update = array(
                "id_faktur_retur" => $this->input->post('id_faktur_retur'),
                "nota" => $this->input->post('nota'),
                "customer" => $this->input->post('customer'),
                // "pertama" => $this->input->post('pertama'),
                // "kedua" => $this->input->post('kedua'),
                // "ketiga" => $this->input->post('ketiga'),
                // "keempat" => $this->input->post('keempat'),
                // "kelima" => $this->input->post('kelima'),
                // "keenam" => $this->input->post('keenam'),
                "jumlah" => $this->input->post('jumlah'),
                // "bg" => $this->input->post('bg'),
                // "total" => $this->input->post('total'),
                // "fak" => $this->input->post('fak'),
                // "ret" => $this->input->post('ret'),
                // "pay" => $this->input->post('pay'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );

            $this->db->where('id', $id);
            $this->db->update('tagihan', $update);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Tagihan berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('tagihan');
        }

    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Tagihan gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('tagihan');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('tagihan');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Tagihan berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('tagihan');
        }
    }
}
