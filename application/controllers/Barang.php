<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("barang_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $barang = $this->barang_m;
        $validation = $this->form_validation;
        $validation->set_rules($barang->rules());
        if ($validation->run()) {
            $barang->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Barang berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("barang");
        }

        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $agen = $this->input->post('agen');
        $excel = $this->input->post('excel');

        if ($tgl_awal == '') $tgl_awal = NULL;
        if ($tgl_akhir == '') $tgl_akhir = NULL;
        if ($agen == '') $agen = NULL;
        
        $data["title"] = "Barang";
        $data["data_agen"] = $this->barang_m->getAgen();
        // print_r($data["agen"]); die();
        $data["data_barang"] = $this->barang_m->getAll($tgl_awal, $tgl_akhir, $agen);

        $data["tgl_awal"] = $tgl_awal;
        $data["tgl_akhir"] = $tgl_akhir;
        $data["agen"] = $agen;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('barang/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('barang/excel', $data);
    }

    public function print()
    {
        $data["data_barang"] = $this->barang_m->getAll();
        $this->load->view('barang/excel', $data);
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('id_agen', 'id_agen', 'required');
        $this->form_validation->set_rules('kd_barang', 'kd_barang', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('artikel', 'artikel', 'required');
        $this->form_validation->set_rules('harga', 'harga', 'required');
        $this->form_validation->set_rules('qty', 'qty', 'required');
        $this->form_validation->set_rules('tgl_masuk', 'tgl_masuk', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Barang gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('barang');
        } else {
            $update = array(
                "id_agen" => $this->input->post('id_agen'),
                "kd_barang" => $this->input->post('kd_barang'),
                "nama" => $this->input->post('nama'),
                "artikel" => $this->input->post('artikel'),
                "harga" => $this->input->post('harga'),
                "qty" => $this->input->post('qty'),
                "tgl_masuk" => $this->input->post('tgl_masuk'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );

            $this->db->where('id', $id);
            $this->db->update('barang', $update);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Barang berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('barang');
        }

    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Barang gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('barang');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('barang');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Barang berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('barang');
        }
    }
}
