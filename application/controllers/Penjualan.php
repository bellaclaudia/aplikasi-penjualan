<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("penjualan_m");
        require_once APPPATH.'third_party/fpdf/fpdf.php';
        
        $pdf = new FPDF();
        $pdf->AddPage();
        
        $CI =& get_instance();
        $CI->fpdf = $pdf;

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $agen = $this->input->post('agen');
        $excel = $this->input->post('excel');

        if ($tgl_awal == '') $tgl_awal = NULL;
        if ($tgl_akhir == '') $tgl_akhir = NULL;
        if ($agen == '') $agen = NULL;

        $data["title"] = "Penjualan";
        $data["data_agen"] = $this->penjualan_m->getAgen();
        $data["data_penjualan"] = $this->penjualan_m->getAll($tgl_awal, $tgl_akhir, $agen);

        $data1["data_penjualan"] = $this->penjualan_m->getExport($tgl_awal, $tgl_akhir, $agen);
        
        $data["tgl_awal"] = $tgl_awal;
        $data["tgl_akhir"] = $tgl_akhir;
        $data["agen"] = $agen;

        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('penjualan/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('penjualan/excel', $data1);

            
    }

    public function tambah($id)
    {
        $data["data_barang"] = $this->penjualan_m->getStokBarang($id);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('penjualan/add', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $checkbox = $this->input->post('id_barang');
        $qty = $this->input->post('qty');
        // print_r($checkbox); die();

        // if($this->input->post('qty') != ''){
        //     $qty = $this->input->post('qty');
        // }else{
        //     $qty = 0;
        // }

        $sqlxx = "SELECT no_kwitansi FROM penjualan order by id desc limit 1";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $urutan    = $hasilxx->no_kwitansi;
            $urutan++;
            $no_kwitansi = sprintf("%03s", $urutan);
        }else{
            $hasilxx = $queryxx->row();
            $urutan = "000";
            $urutan++;
            $no_kwitansi = sprintf("%03s", $urutan);
        }
        $sisa_stok = 0;
        $jumlah = 0;

            foreach ($checkbox as $key => $value) {
                // print_r($checkbox[$key]); die();
                $sqlxx = "SELECT id, stok FROM stok where id = '$checkbox[$key]'";
                $queryxx = $this->db->query($sqlxx);
                if ($queryxx->num_rows() > 0) {
                    $hasilxx = $queryxx->row();
                    $stok    = $hasilxx->stok;
                }

                $sisa_stok = $stok - $qty[$key];

                // update data di krs_mhs
                $tgl_skrg = date('Y-m-d H:i:s');
                $data = array(
                    'stok' => $sisa_stok,
                    'tgl_update' => $tgl_skrg,
                    'user_update_by' => $this->session->userdata['username']
                );
                $this->db->where('id', $checkbox[$key]);
                $this->db->update('stok', $data);

                $sqlxx = " select * from stok b WHERE b.id = '$checkbox[$key]' ";
                $queryxx = $this->db->query($sqlxx);
                if ($queryxx->num_rows() > 0) {
                    $hasilxx = $queryxx->row();
                    $harga    = $hasilxx->harga;
                    $id_agen    = $hasilxx->id_agen;
                }

                $jumlah = $qty[$key] * $harga;

                $this->penjualan_m->insert(array(
                    "id_barang" => $checkbox[$key],
                    "nama_sales" => $this->input->post('nama_sales'),
                    "nama_pembeli" => $this->input->post('nama_pembeli'),
                    "alamat" => $this->input->post('alamat'),
                    "qty" => $qty[$key],
                    "jumlah" => $jumlah,
                    "tgl_pembelian" => date('Y-m-d'),
                    "no_kwitansi" => $no_kwitansi,
                    "tgl_input" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                ));

                $id_nya = $this->db->insert_id();

                $this->db->delete('penjualan', array('jumlah'=>0));

                // $sqlxx = " select b.id_agen, a.nama_pembeli, sum(a.jumlah) as total, a.no_kwitansi from penjualan a left join stok b on b.id = a.id_barang left join agen c on c.id = b.id_agen WHERE a.no_kwitansi = '$no_kwitansi' group by a.no_kwitansi";
                // $queryxx = $this->db->query($sqlxx);
                // if ($queryxx->num_rows() > 0) {
                //     $hasilxx = $queryxx->row();
                //     $id_agen2    = $hasilxx->id_agen;
                //     $total    = $hasilxx->total;
                //     $no_kwitansi2    = $hasilxx->no_kwitansi;
                //     $nama_pembeli    = $hasilxx->nama_pembeli;
                // }

                $data = array(
                    "id_agen" => $id_agen,
                    "nota" => $no_kwitansi,
                    "jumlah" => $jumlah,
                    "customer" => $this->input->post('nama_pembeli'),
                    "tgl_input" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );
                $this->db->insert('tagihan', $data);

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            }

        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Data Pembelian berhasil disimpan. 
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button></div>');
        redirect('penjualan/pdf/'.$no_kwitansi);
    }

    public function pdf($no_kwitansi)
    {
        $no=1;
        $data['invoice'] = $this->penjualan_m->getOne($no_kwitansi)->row_array();
        $data['data_invoice'] = $this->penjualan_m->getOne($no_kwitansi)->result_array();
        $this->load->view('penjualan/pdf',$data);
    }

    // public function edit($id = null)
    // {
    // 	$data["data_barang"] = $this->penjualan_m->getBarang();
    //     $this->form_validation->set_rules('id_barang', 'id_barang', 'required');
    //     $this->form_validation->set_rules('qty', 'qty', 'required');
    //     $this->form_validation->set_rules('tgl_pembelian', 'tgl_pembelian', 'required');
    //     $this->form_validation->set_rules('no_kwitansi', 'no_kwitansi', 'required');
    //     if ($this->form_validation->run() == FALSE) {
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data Penjualan gagal diedit. 
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect('penjualan');
    //     } else {
    //     	$id_barang = $this->input->post('id_barang');
	   //  	$sqlxx = " select * from barang b WHERE b.id = '$id_barang' ";
	   //      $queryxx = $this->db->query($sqlxx);
	   //      if ($queryxx->num_rows() > 0) {
	   //          $hasilxx = $queryxx->row();
	   //          $harga    = $hasilxx->harga;
	   //      }

	   //      $jumlah = $this->input->post('qty') * $harga;

    //         $data = array(
    //             "id" => $this->input->post('id'),
    //             "id_barang" => $this->input->post('id_barang'),
    //             "qty" => $this->input->post('qty'),
    //             "jumlah" => $jumlah,
    //             "tgl_pembelian" => $this->input->post('tgl_pembelian'),
    //             "no_kwitansi" => $this->input->post('no_kwitansi'),
    //             "tgl_update" => date('Y-m-d H:i:s'),
    //             "user_update_by" => $this->session->userdata['username']
    //         );
    //         $this->db->where('id', $_POST['id']);
    //         $this->db->update('penjualan', $data);
    //         $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    //         Data Penjualan berhasil diedit. 
    //         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //         <span aria-hidden="true">&times;</span>
    //         </button></div>');
    //         redirect('penjualan');
    //     }
    // }

    public function edit($no_kwitansi = null)
    {
        if (!isset($no_kwitansi)) redirect('penjualan');

        $penjualan = $this->penjualan_m;
        $validation = $this->form_validation;
        $validation->set_rules($penjualan->rules());

        if ($validation->run()) {
            $this->penjualan_m->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Penjualan berhasil diedit.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("penjualan");
        }
        $data["title"] = "Edit Data Penjualan";
        $data["data_barang"] = $this->penjualan_m->getBarang();
        $data["data_penjualan"] = $this->penjualan_m->getById($no_kwitansi);
        if (!$data["data_penjualan"]) show_404();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('penjualan/edit', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($no_kwitansi)
    {
        if ($no_kwitansi == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Penjualan gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('penjualan');
        } else {
            $this->db->where('no_kwitansi', $no_kwitansi);
            $this->db->delete('penjualan');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Penjualan berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('penjualan');
        }
    }

    public function invoice($no_kwitansi){
    $no=1;
    $data = $this->penjualan_m->getOne($no_kwitansi)->row_array();
    
    $no_kwitansi = $data['no_kwitansi'];
    $agen = $data['agen'];

    $pdf = new FPDF('L','mm','A5'); 
    $pdf->SetMargins(10 ,10, 10 );
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',13);
    $pdf->Cell(0,7,''.$agen,0,1,'C');
    $pdf->SetFont('Arial','B',13);
    $pdf->Cell(0,7,'Bantarujeg',0,1,'C');
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell(0,7,'',0,1,'C');
    $pdf->Cell(0,7,'Telp. 0822-99961116',0,1,'C');
    $pdf->SetFont('Arial','B',10);
    $pdf->SetLineWidth(1);
    $pdf->SetDrawColor(1,1,1);
    $pdf->line(10,40,140,40);
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(15,5,'',0,0,'L');
    $pdf->Cell(15,5,'',0,0,'L');
    $pdf->Cell(70,5,'',0,1,'L');
    $pdf->Cell(0,7,'No. Kwitansi : '.$no_kwitansi,0,1,'L');
    $pdf->SetFont('Arial','B',8);
    $pdf->SetFont('Arial','',8);
    $pdf->Cell(10,8,'Qty',1,0,'C');
    $pdf->Cell(55,8,'Nama Barang',1,0,'C');
    $pdf->Cell(30,8,'Harga',1,0,'C');
    $pdf->Cell(35,8,'Jumlah',1,0,'C');
    $pdf->Ln();
    $query=$this->penjualan_m->getOne($no_kwitansi);
    $jumlah = 0;
    foreach ($query->result_array() as $row)
    {
    $jumlah += $row['jumlah'];
    $pdf->Cell(10,8,$row['qty'],1,0,'C');
    $pdf->Cell(55,8,$row['nama_barang'],1,0,'L');
    $pdf->Cell(30,8,"Rp " . number_format($row['harga'],2,',','.'),1,0,'L');
    $pdf->Cell(35,8,"Rp " . number_format($row['qty']*$row['harga'],2,',','.'),1,0,'L');
    $pdf->Ln();
    }
    $pdf->Cell(10,8,'',1,0,'C');
    $pdf->Cell(55,8,'',1,0,'L');
    $pdf->Cell(30,8,'',1,0,'L');
    $pdf->Cell(35,8,"Rp " . number_format($jumlah,2,',','.'),1,0,'L');
    $pdf->Output();
    }  

    function pilihpegawai(){
        $key=$_POST['q'];
        // $key='alfan';
        $ar=$this->penjualan_m->pilihPegawai($key)->result_array();
        $json=array();
        foreach ($ar as $r){
            array_push($json,array('id'=>$r['id'], 'text'=> $r['nama'].' | '. $r['kd_barang'].' | '.$r['artikel'].' | '.$r['stok']));
        }
        $data=json_encode($json);
        print_r($data);
    }  
}
